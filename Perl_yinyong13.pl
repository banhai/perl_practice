#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
=cut
引用就是指针，Perl 引用是一个标量类型可以指向变量、数组、哈希表（也叫关联数组）
甚至子程序，可以应用在程序的任何地方。
=cut
$scalarref = \$foo;     # 标量变量引用
$arrayref  = \@ARGV;    # 列表的引用
$hashref   = \%ENV;     # 哈希的引用
$coderef   = \&handler; # 子过程引用
$globref   = \*foo;     # GLOB句柄引用
$aref= [ 1,"foo",undef,13 ];#匿名数组引用
my $aref = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
]; #构造数组的数组，构造多个维度的数组
$href= { APR =>4, AUG =>8 };#匿名的哈希引用
$coderef=sub{print("hello world!\n");};
print(&$coderef,"\n");
#取消引用 可以根据不同的类型使用 $, @ 或 % 来取消
$var =10;
$r=\$var;
print("原始$var,转换成引用$r,还原成最初的模样$$r\n");
@arr =(10,11,12,13);
$ra=\@arr;
print("原始@arr,转换成引用$ra,还原成最初的模样@$ra\n");
# 自己编写
%val=('name' => "张三",'age' => 20);
$ra=\%val;
print("原始,",%val,"转换成引用$ra,还原成最初的模样",%$ra,"\n");#哈希的输出不讲究规则
#案例
%var = ('key1' => 10,'key2' => 20);
# $r 引用  %var 哈希
$r = \%var;
# 输出本地存储的 $r 的变量值
print "\%var 为 : ", %$r, "\n";
#如果不能确定返回值可以使用ref函数来进行判断
=cut
SCALAR
ARRAY
HASH
CODE
GLOB
REF
=cut
print("=====引用类型======\n");
$var=10;
$r=\$var;
print "r的引用类型：",ref($r),"\n";
@var = (1, 2, 3);
$r=\@var;
print "r的引用类型：",ref($r),"\n";
%var = ('key1' => 10, 'key2' => 20);
$r = \%var;
print "r 的引用类型 : ", ref($r), "\n";
#循环引用：在两个引用相互包含时出现需要小心，不然会导致内存泄露。
my $foo=100;
$foo=\$foo;
print("value of foo is:",$$foo,"\n");
#引用函数
sub PrintHash{
    my(%hash)=@_;
    foreach $item (%hash){
        print("元素：$item\n");
    }
}
%hash=('name'=>'张三','age'=>4);
#创建函数的引用
$cref=\&PrintHash;##为什么从后面输出
#使用引用调用函数
&$cref(%hash);