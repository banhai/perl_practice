#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;
binmode(STDOUT,":encoding(gbk)");
#use strict;#让所有变量需要强制声明类型
my $age=25;
my $name="Tom";
my $salary=1245.26;
print "$age\n";
print '$name\n';
print "$salary\n";
#标量变量
my @ages=(20,23,25,36);
my @names=("张三","李四","王五","马六");
#数组 @ 开始。 要访问数组的变量，可以使用美元符号($)+变量名，并指定下标来访问
print "\$ages[0]=$ages[0]\n";
print "\$ages[1]=$ages[1]\n";
print "\$ages[2]=$ages[2]\n";
print "\$ages[3]=$ages[3]\n";
print "----------------\n";
print "\$names[0]=$names[0]\n";
print "\$names[1]=$names[1]\n";
print "\$names[2]=$names[2]\n";
print "\$names[3]=$names[3]\n";
print "----------------\n";
#哈希变量
my %data=('name','tom','age',24,'habby','no');
print"\$data{'name'}=$data{'name'}\n";
print"\$data{'age'}=$data{'age'}\n";
print"\$data{'habby'}=$data{'habby'}\n";

=start
变量的上下文：值的是表达式所在的位置。
上下文是由等号左边的变量类型决定的，等号左边是标量，则是标量上下文，等号左边是列表，则是列表上下文。Perl 解释器会根据上下文来决定变量的类型
=cut


my @names1 = ('google', 'runoob', 'taobao');
my @copy = @names1;   # 复制数组
my $size = @names1;   # 数组赋值给标量，返回数组元素个数
print "名字为 : @copy\n";#第一个将其复制给另外一个数组，所以输出了数组所有的元素，第二个我们将数组赋值给一个标量，它返回数组的元素个数。
print "名字数：$size\n";
#第一个将其复制给另外一个数组，所以输出了数组所有的元素，第二个我们将数组赋值给一个标量，它返回数组的元素个数。

=begin----不是很理解
1 	标量 −
赋值给一个标量变量，在标量上下文的右侧计算
2 	列表 −
赋值给一个数组或哈希，在列表上下文的右侧计算。
3 	布尔 −
布尔上下文是一个简单的表达式计算，查看是否为 true 或 false。
4 	Void −
这种上下文不需要关系返回什么值，一般不需要返回值。
5 	插值 −
这种上下文只发生在引号内。
=cut

