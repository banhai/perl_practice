use utf8;
binmode(STDOUT,":encoding(gbk)");

#这是一个单行的注释

=start
这是一个多行的注释，不需要进行配置
=cut
print "您好,世界\n";#简单的输出
print "hello
            world\n"   ;#这是一个掺杂了换行的输出，所有的空格，tab,空行，等如果在引号外解释器忽略，如果在引号内会原样输出。

#单双引号的使用可以转义一些字符
print "hello world\n";
print 'hello world\n';

$a=10;#配置一个标量
print "a= $a\n";
print 'a= $a \n';#单引号无法正常识别特殊符号
