#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
$var = 10;
$r = \$var;
# 输出本地存储的 $r 的变量值
print "$var 为 : ", ${$r}, "\n";
@var = (1, 2, 3);
# $r 引用  @var 数组
$r = \@var;
# 输出本地存储的 $r 的变量值
print "@var 为: ",  @{$r}, "\n";

%var = ('key1' => 10, 'key2' => 20);
$r = \%var;
# 输出本地存储的 $r 的变量值
print "\%var 为 : ", %{$r}, "\n";
my @array2 = [1, 2, 3, 4, 5];
print @array2; # e.g. "ARRAY(0x182c180)"
my $abc={
    'name'=>'张三',
    'age'=>15
};
print($abc);
print("\n");
my $scalar = (Alpha, Beta, Gamma, Pie);
print $scalar; # "Pie"此处返回的是最后一个索引值如果需要返回引用需要使用方括号
print("\n");
my $word = "antidisestablishmentarianism";
my @ad=(1,2,3,45,8,7,46,5,56);
my $strlen = length $word;
print($strlen."\n");
print(scalar @ad);
print("\n");
my $eggs = 5;
print "You have ", $eggs == 0 ? "no eggs" :
    $eggs == 1 ? "an egg"  :
        "some eggs";
print("\n");
print("========map$_函数=======\n");
#该函数将数组作为输入，并将运算应用于此数组中的每个标量。然后，它从结果中构造一个新数组。要执行的操作以大括号内单个表达式的形式提供
my @capitals = ("Baton Rouge", "Indianapolis", "Columbus", "Montgomery", "Helena", "Denver", "Boise");
print join ", ", map { uc $_ } @capitals;
#该函数将数组作为输入，并返回经过筛选的数组作为输出。语法类似于 。这一次，为输入数组中的每个标量计算第二个参数。如果返回布尔值 true 值，则将标量放入输出数组中，否则不会。grepmap$_
print join ", ", grep { length $_ == 6 } @capitals;
# "Helena, Denver"
