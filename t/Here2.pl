#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;
binmode(STDOUT,":encoding(gbk)");
=start
Here文档又称作heredoc、hereis、here-字串或here-脚本，是一种在命令行shell（如sh、csh、ksh、bash、PowerShell和zsh）
和程序语言（像Perl、PHP、Python和Ruby）里定义一个字串的方法。
使用概述：
    1.必须后接分号，否则编译通不过。
    2.END可以用任意其它字符代替，只需保证结束标识与开始标识一致。
    3.结束标识必须顶格独自占一行(即必须从行首开始，前后不能衔接任何空白和字符)。
    4.开始标识可以不带引号号或带单双引号，不带引号与带双引号效果一致，解释内嵌的变量和转义符号，带单引号则不解释内嵌的变量和转义符号。
    5.当内容需要内嵌引号（单引号或双引号）时，不需要加转义符，本身对单双引号转义，此处相当与q和qq的用法。
=cut
$a =  10;
my $var = <<"EOF";
这是一个Here文档，使用双引号。
可以在这里输入字符串和变量。
例如：a=$a
这是'和"不需要进行转义符号\\ 但是输出特殊符号\$需要转义使用转义符号"\\"
EOF
print "$var\n";

$var = <<'EOF';
这里是一个Here文档，使用的是单引号。
例如：a=$a
EOF
print "$var\n";

#转义字符
my $result="菜鸟教程 \"runoob\"";
print "$result\n";
print '\$result\n';#不能转义和换行
print "\$result\n";
print '您好,世界\\n';#转义字符不生效


=start2
 Perl 标识符是用户编程时使用的名字，在程序中使用的变量名，常量名，函数名，语句块名等统称为标识符。
    标识符组成单元：英文字母（a~z，A~Z），数字（0~9）和下划线（_）。
    标识符由英文字母或下划线开头。
    标识符区分大小写，$runoob 与 $Runoob 表示两个不同变量。
=cut
