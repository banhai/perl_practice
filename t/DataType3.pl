#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use utf8;
binmode(STDOUT,":encoding(gbk)");
#标量标量是 Perl 语言中最简单的一种数据类型。这种数据类型的变量可以是数字，字符串，浮点数，不作严格的区分。在使用时在变量的名字前面加上一个 $，表示是标量
my $myfirst=123;
my $myname="Tom";
#数组：数组变量以字符 @ 开头，索引从 0 开始，如：@arr=(1,2,3)
my @arr=(1,2,3,4);
my @arrt=("张三","李四","王五");
#哈希：哈希是一个无序的 key/value 对集合。可以使用键作为下标获取值。哈希变量以字符 % 开头
my %h=('a'=>1,'b'=>2,'c'=>3);

#数字字面量
#Perl 实际上把整数存在你的计算机中的浮点寄存器中，所以实际上被当作浮点数看待。在多数计算机中，浮点寄存器可以存贮约 16 位数字，长于此的被丢弃。整数实为浮点数的特例
my $x=123456;
if(1217+116==1333){
    print "这是整型\n";
}
#进制数：8进制以0开始，16进制以0x开始
my $var1=047;#等于十进制的39
my $var2=0x1f;#等于十进制的31
print "$var1,$var2\n";
#浮点数 如：11.4，12.5，.3，3.，54.1e=02,5.41e03
my $value = 9.01e+21 + 0.01 - 9.01e+21;
print ("第一个值为：", $value, "\n");
my $value2 = 9.01e+21 - 9.01e+21 + 0.01;
print ("第二个值为:", $value2, "\n");
#输出结果与教程不对，不理解
#字符串
my $varstring='这是一个
多行字符串文本
的例子';
print($varstring);
#Perl中常用的转义字符
=start
\\ 	反斜线
\' 	单引号
\" 	双引号
\a 	系统响铃
\b 	退格
\f 	换页符
\n 	换行
\r 	回车
\t 	水平制表符
\v 	垂直制表符
\0nn 	创建八进制格式的数字
\xnn 	创建十六进制格式的数字
\cX 	控制字符，x可以是任何字符
\u 	强制下一个字符为大写
\l 	强制下一个字符为小写
\U 	强制将所有字符转换为大写
\L 	强制将所有的字符转换为小写
\Q 	将到\E为止的非单词（non-word）字符加上反斜线
\E 	结束\L、\U、\Q
=cut

# 只有 R 会转换为大写
my $str1 = "\urunoob";
print "$str1\n";

# 所有的字母都会转换为大写
my $str2 = "\Urunoob";
print "$str2\n";

# 指定部分会转换为大写
my $str3 = "Welcome to \Urunoob\E.com!";
print "$str3\n";

# 将到\E为止的非单词（non-word）字符加上反斜线
my $str4 = "\QWelcome to runoob's family";
print "$str4\n";

# 将到\E为止的非单词（non-word）字符加上反斜线
my $str5 = "\QWelcome to runoob's\Efamily";
print "$str5\n";