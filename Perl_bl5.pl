#!/usr/bin/perl
use warnings FATAL => 'all';
use utf8;
#存在一个问题，定义一个标量或者是变量都必须带上 my标识符?问题解决：使用了use strict;
binmode(STDOUT,":encoding(gbk)");
my $age = 20;             # 整数赋值
my $name = "Runoob";   # 字符串
my $salary = 130.50;     # 浮点数

print "Age = $age\n";
print "Name = $name\n";
print "Salary = $salary\n";


$var = "字符串标量 - 菜鸟教程!";
$quote = '我在单引号内 - $var';
$double = "我在双引号内 - $var";
$escape = "转义字符使用 -\tHello, World!";
print "var = $var\n";
print "quote = $quote\n";
print "double = $double\n";
print "escape = $escape\n";
#标量运算
$str="您好"."世界";
$num=5-10;
$mul=3*5;
$mix=$str.$mul;
print "\$str=$str\n";
print "\$num=$num\n";
print "\$mix=$mix\n";
#多行字符串的输出
$str='你好
张三
我是李四
';
print "\$str=$str";
#使用Here文档
$var=<<"str";
    这是一个多行文本,可以输出多行的文本
    这是张三
    这是李四
str
print "\$var=$var";

#特殊的字符:这些特殊字符是单独的标记，不能写在字符串中
print "文件名".__FILE__."\n";
print "行名".__LINE__."\n";
print "包名".__PACKAGE__."\n";

#v字符串 这一部分不是很理解****************************
$smile  = v9786;
$foo    = v102.111.111;
$martin = v77.97.114.116.105.110;

print "smile = $smile\n";
print "foo = $foo\n";
print "martin = $martin\n";
