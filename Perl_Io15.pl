#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
=cut
Perl的文件操作
Perl 使用一种叫做文件句柄类型的变量来操作文件。
从文件读取或者写入数据需要使用文件句柄。
文件句柄(file handle)是一个I/O连接的名称。
Perl提供了三种文件句柄:STDIN,STDOUT,STDERR，分别代表标准输入、标准输出和标准出错输出。
Perl 中打开文件可以使用以下方式：

open FILEHANDLE, EXPR
open FILEHANDLE
sysopen FILEHANDLE, FILENAME, MODE, PERMS
sysopen FILEHANDLE, FILENAME, MODE

FILEHANDLE：文件句柄，用于存放一个文件唯一标识符。
EXPR：文件名及文件访问类型组成的表达式。
MODE：文件访问类型。
PERMS：访问权限位(permission bits)。
=cut

#使用open函数以只读的方式(<)打开文件file.txt;
#open(DATA,"<file.txt");  DATA 为文件句柄用于读取文件 是不是作为唯一标识
# open(dada,"<file.txt") or die "flie.txt 文件无法打开，$!";
# while(<dada>){
#     print "$_";
# }

#1.以写入的(>的方式打开文件file.txt:
#open(data,">file3.txt") or die "file.txt文件无法打开,$!"
#2.表示写入方式：如果需要读写方式打开文件，可以在>或<字符前面添加+号
#open(DATA,"+<file.txt") or die("file.txt 文件无法打开,$!");
#这种方式不会删除文件原来的内容，如果要删除，格式如下所示
#open Data,"+>file.txt" or die "file.txt文件无法打开,$!";
#如果向文件中追加数据，则在追加数据之前，只需要以追加凡是打开文件即可
#open(DATA,">>file.txt")|| die "file.txt 文件无法代开,$!";
#>> 表示向现有文件的尾部追加数据，如果需要读取要追加的文件内容可以添加 + 号：
#open(DATA,"+>>file.txt") || die "file.txt 文件无法打开, $!";

=cut
< 或 r	    只读方式打开，将文件指针指向文件头。
> 或 w	    写入方式打开，将文件指针指向文件头并将文件大小截为零。如果文件不存在则尝试创建之。
>> 或 a	    写入方式打开，将文件指针指向文件末尾。如果文件不存在则尝试创建之。
+< 或 r+	读写方式打开，将文件指针指向文件头。
+> 或 w+	读写方式打开，将文件指针指向文件头并将文件大小截为零。如果文件不存在则尝试创建之。
+>> 或 a+	读写方式打开，将文件指针指向文件末尾。如果文件不存在则尝试创建之。
=cut
#print("====Sysopen函数========\n");
#Sysopen函数类似于open函数，只是他们的参数形式不一样
#以下实例是以读写(+<filename)的凡是打开文件
#sysopen(DATA,"file.txt",O_RDWR);
#如果需要在更新文件前清空文件，则写法如下
#sysopen(DATA,"file.txt",O_RDWR|O_TRUNC);
#你可以使用 O_CREAT 来创建一个新的文件， O_WRONLY 为只写模式， O_RDONLY 为只读模式。
# the Perms 参数为八进制属性值，表示文件长i教案后的权限，默认为0x666.

=cut
O_RDWR	    读写方式打开，将文件指针指向文件头。
O_RDONLY	只读方式打开，将文件指针指向文件头。
O_WRONLY	写入方式打开，将文件指针指向文件头并将文件大小截为零。如果文件不存在则尝试创建之。
O_CREAT	    创建文件
O_APPEND	追加文件
O_TRUNC	    将文件大小截为零
O_EXCL 	    如果使用O_CREAT时文件存在,就返回错误信息,它可以测试文件是否存在
O_NONBLOCK	非阻塞I/O使我们的操作要么成功，要么立即返回错误，不被阻塞。
=cut
#Close函数文件使用完后，要关闭文件，以刷新与句柄相关联的输入输出缓冲区，关闭文佳
#  close FILEHANDLE
#  close
#FILEHANDLE 为指定的文件句柄，如果成功关闭则返回 true。
#close(DATA) || die "无法关闭文件";

# print(+++++++++读写文件+++++++)
#<FILEHANDL>操作符
# 从打开的文件句柄读取信息的主要方法是<FILEHANDLE>操作符。在标量上下文中，它从文件句柄返回一行
=cut
print"菜鸟教程网址？\n";
$name=<STDIN>;#输入语句用于向控制台输入内容
print("网址：$name\n");
=cut

#当我们使用<FILEHANDLE>操作符时，它会返回文件句柄中每一行的列表，例如我们可以导入素有的行到数组中。
#实现创建 import.txt文件，内容如下
=cut
$ cat import.txt
1
2
3
=cut
#读取import.txt并将每一行放到@lines数组中
# open (Data,"<import.txt") or die "无法打开数据";
# @lines=<DATA>;
# print(@lines);
# close(Data);
# 只读方式打开文件
# open(DATA1, "<file.txt");
# # 打开新文件并写入
# open(DATA2, ">file2.txt");
# # 拷贝数据
# while(<DATA1>)
# {
#     print("执行");
#     print DATA2 $_;
# }
# close( DATA1 );
# close( DATA2 );

#文件重命名
#我们将已存在的文件 file1.txt 重命名为 file2.txt，指定的目录是在 /usr/runoob/test/ 下
# rename ("/usr/runoob/test/file1.txt", "/usr/runoob/test/file2.txt" );
# 函数renames只接收两个参数，只对已存在的文件进行重命名
unlink("/Usr/runoob/test/file1.txt");
print("======打住==========");