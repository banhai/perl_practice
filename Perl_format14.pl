#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
=cut
Perl是一个强大的文本处理语言
perl中可以使用format来定义一个模板，然后使用write按指定模板输出数据
语法格式


fomat ForamteName=
fieldline
value_one,value_two,value_three
fieldline
value_one,value_two
参数解析：
FormatName:格式化名称
fieldine:一个格式，用来定义一个输出的格式，类似@，^,<,>,|这样的字符。
value_one,value_two.....：数据行，用来向前面的格式行中插入值，都是perl的变量
.  : 结束的符号
=cut
print("====简单格式化实例======\n");
$text="google taobao bidu ";
#使用时注意索引从0开始，同时空格也计算入内
format STDOUT=
first: @<<<<<<<<<<<<<<
    $text
second: ^<<<<<
    $text
third: ^<<<<
    $text
.
write;


# print("=====教程中的案例======\n");
# $text2 = "google runoob taobao";
# format STDOUT =
# first: ^<<<<<  # 左边对齐，字符长度为6这里字符串长度不正确
#     $text2
# second: ^||||| # 左边对齐，字符长度为6
#     $text2
# third: ^<<<< # 左边对齐，字符长度为5，taobao 最后一个 o 被截断
#     $text2
# .
# write;

=cut
格式行（图形行）语法
1. 格式行以 @ 或者 ^ 开头，这些行不作任何形式的变量代换。
2. @ 字段(不要同数组符号 @ 相混淆)是普通的字段。
3. @,^ 后的 <, >,| 长度决定了字段的长度，如果变量超出定义的长度,那么它将被截断。
4. <, >,| 还分别表示,左对齐,右对齐,居中对齐。
5.  ^ 字段用于多行文本块填充。

值域格式，如下所示
1. @<<< 	左对齐输出
2. @>>> 	右对齐输出
3. @||| 	中对齐输出
4. @##.##   固定精度数字
5. @* 	     多行文本

每个值域的第一个字符是填充符，当使用@字符时，不做文本格式化
除了多行值域@*,域宽都等于其指定的包含字符@在内的字符个数
如 @###.##
=cut

format EMPLOYEE=
=================
@<<<<<<<<<<<<<<<<< @<<
$name,$age
@######.#
$salary
==================
.
select(STDOUT);
$~=EMPLOYEE;
@n=("Ali","Runoob","Jaffer");
@a=(20,30,40);
@s=(2000.00,2500,00,4000.000);
$i=0;
foreach(@n){
    $name=$_;
    $age=$a[$i];
    $salary=$s[$i++];
    write;
}


