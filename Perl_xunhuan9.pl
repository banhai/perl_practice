#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
use warnings FATAL => 'all';

=start
注意，数字 0, 字符串 '0' 、 "" , 空 list () ,
和 undef 为 false ，其他值均为 true。 true 前面使用 ! 或 not则返回 false 。
=cut
#while循环的语句
$a=10;
while($a<20){
    printf "a 的值为 : $a\n";
    $a=$a + 1;
}
print("======分割线======\n");
#until语句相当于给定条件false时，重复执行语句或者语句组 --译为除非
#相当于if条件语句里面的unless
until($a>20){
    printf "a 的值为：$a\n";
    $a=$a+1;
}
#for循环的任务
=kaishi
for ( init; condition; increment ){
   statement(s);
}
1. init 会首先被执行，且只会执行一次。这一步允许您声明并初始化任何循环控制变量。您也可以不在这里写任何语句，只要有一个分号出现即可。
2. 接下来，会判断 condition。如果为 true，则执行循环主体。如果为 false，则不执行循环主体，且控制流会跳转到紧接着 for 循环的下一条语句。
3. 在执行完 for 循环主体后，控制流会跳回上面的 increment 语句。该语句允许您更新循环控制变量。该语句可以留空，只要在条件后有一个分号出现即可。
4. 条件再次被判断。如果为 true，则执行循环，这个过程会不断重复（循环主体，然后增加步值，再然后重新判断条件）。在条件变为 false 时，for 循环终止。
=cut
print("=======分割线=======\n");
# 执行 for 循环
for($a = 0;$a < 10;$a = $a+1){
    print "a 的值为: $a\n";
}
#foreach 循环用于迭代一个列表或集合变量的值
@list=(1,2,3,4,5,6,7,8,9,10);
foreach $item(@list){
    print("$item\n");
}
print("==========分割线======\n");
#do....while循环与其他do while相似确保执行一次循环
$a = 10;
# 执行 do...while 循环
do{
    printf "a 的值为: $a\n";
    $a = $a + 1;
}while( $a < 15 );
print("==========分割线======\n");
#嵌套循环
for($a=1;$a<=9;$a++){
    for($b=1;$b<=9;$b++){
        printf("$a*$b=".$a*$b."\n");
    }
}
#自留任务循环控制语句输出乘法口诀
#Perl循环用于停止执行从next语句的下一语句开始到循环体结束标识符之间的语句，转去执行continue语句块，然后再返回到循环体的起始处开始执行下一次循环。
#next[LABEL];
$a = 10;
while( $a < 20 ){
    if( $a == 15)
    {
        # 跳出迭代
        $a = $a + 1;
        next;
    }
    print "a 的值为: $a\n";
    $a = $a + 1;
}
print("====分割线=======\n");
#last语句用于退出循环语句快，从而结束循环，last语句之后的语句不在执行，continue语句也不在执行
$a = 10;
while( $a < 20 ){
    if( $a == 15)
    {
        # 退出循环
        $a = $a + 1;
        last;
    }
    print "a 的值为: $a\n";
    $a = $a + 1;
}
#Perl continue块通常在条件语句再次判断前执行。 continue用在while和foreach循环中

=start
//while中使用continue
while(condition){
   statement(s);
}continue{
   statement(s);
}
//foreach中使用continue
foreach $a (@listA){
   statement(s);
}continue{
   statement(s);
}
=cut
print("======分割线=====\n");
$a = 0;
while($a < 3){
    print "a = $a\n";
}continue{#语句块通常在条件语句再次判断前执行
    $a = $a + 1;
    print("这是最后一次执行后的continue$a\n");
}
print("=====分割线======\n");
@list = (1,2,3,4,5);
foreach $a (@list){
    print "a = $a\n";
}continue{
    last if $a == 4;#last 退出循环也相当于结束循后面是条件
}
#redo语句
#Perl语句直接转到循环第一行开始重复执行本次循环，redo语句之后的语句不再执行，continue语句块也不再执行。
=zh
语法格式
redo [LABEL]
带标号修饰符LABEL的redo语句表示把循环控制流程直接转到与标号修饰符LABEL相关联的语句块的第一行处开始执行，而不再执行redo语句之后的语句和continue语句块；
不带标号修饰符LABEL的redo语句表示把循环控制流程直接转到当前语句块的第一行处开始执行，而不再执行redo语句之后的语句和continue语句块；
如果是在for循环中或者是带有continue语句块，则for循环中的递增列表和continue语句块都不再被执行；
=cut
print("=====分割线=====\n");
$a = 0;
while($a < 10){
    if( $a == 5 ){
        $a = $a + 1;
        redo;
    }
    print "a = $a\n";
}continue{
    $a = $a + 1;
}
print("====分割线=====\n");
=goto
goto类型：
1	goto LABEL
找出标记为 LABEL 的语句并且从那里重新执行。
2	goto EXPR
goto EXPR 形式只是 goto LABEL 的一般形式。它期待表达式生成一个标记名称，并跳到该标记处执行。
3	goto &NAME
它把正在运行着的子进程替换为一个已命名子进程的调用。
=cut

$a=10;
LOOP:
do
{
    if($a==15){
        #跳过迭代
        $a=$a+1;
        #使用goto LABEL形式
        print("跳出循环\n");
        goto LOOP;#类似于redo的功能
        print "这一句不会被执行\n";
    }
    print("a=$a\n");
    $a=$a+1;
}while($a<20);
print("======分割线======\n");

$b=10;
$str1="da";
$str2="da";
dada:do
{
    if($b==15){
        #跳过迭代
        $b=$b+1;
        #使用goto EXPR形式
        goto $str1.$str2;#类似 goto LOOP
    }
    print "a=$b\n";
    $b=$b+1;
}while($b<20);
#死循环：条件表达式不存在时，他被假设为true,您也可以设置一个初始值和增量表达式，但是一般情况下，
# Perl程序员偏向于使用for(;;;)来表示死循环
for(;;){
    printf"循环无限执行。\n";
}