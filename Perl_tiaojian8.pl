#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
use feature qw(switch);
=start
注意，数字 0,字符串 '0' 、"" ,空 list(),和 undef为false,其他值均为 true。
true 前面使用!或 not则返回 false。
=cut
use warnings FATAL => 'all';
#if语句来进行学习
$a=10;
if ($a<20) {
    print("输出就是true\n");
}else{
    print("输出就是false\n");
}
if(1){#不识别关键子true,false;
    print("这就是最后的结果true\n");
}
if (0) {
    print("真\n");
}elsif(-52){ #另一个条件分支不带e 关键子为elsif 0为false,除0外都为true
    print("第二个条件为真\n");
}else{
    print("都不成立的情况下进行\n");
}
print("=====unless====语句\n");
unless(0){#当条件是false的情况下执行
    print("这是在否则的条件下进行的\n");
}

$a = 100;
# 使用 unless 语句检测布尔表达式
unless( $a == 20 ){
    # 布尔表达式为 false 时执行
    printf "给定的条件为 false\n";
}else{
    # 布尔表达式为 true 时执行
    printf "给定的条件为 true\n";
}
print "a 的值为 : $a\n";
=go
一个 unless 语句后可跟一个可选的 elsif 语句，然后再跟另一个 else 语句。
这种条件判断语句在多个条件的情况下非常有用。
在使用 unless , elsif , else 语句时你需要注意以下几点。
  unless 语句后可以跟上 0 个 或 1 个 else 语句，但是 elsif 后面必须有 else 语句。
  unless 语句后可以跟上 0 个 或 1 个 elsif 语句，但它们必须写在 else 语句前。
  如果其中的一个 elsif 执行成功，其他的 elsif 和 else 将不再被执行。
=cut
$a = 30;
# 使用 unless 语句检测布尔表达式
unless( $a  ==  30 ){
    # 布尔表达式为 false 时执行
    printf "a 的值不为 30\n";
}elsif( $a ==  30 ){#相当与if语句
    # 布尔表达式为 true 时执行
    printf "a 的值为 30\n";
}else{
    # 没有条件匹配时执行
    printf "a的值为===$a\n";
}
#新版本的perl中引入switch语句 需要引入关键字 use switch

$var = 10;
@array = (10, 20, 30);
%hash = ('key1' => 10, 'key2' => 20);
=bug
switch($var){
    case 10           { print "数字 10\n"; next; }  # 匹配后继续执行
    case "a"          { print "string a" }
    case [1..10,42]   { print "数字在列表中" }
    case (\@array)    { print "数字在数组中" }
    case (\%hash)     { print "在哈希中" }
    else              { print "没有匹配的条件" }
}
=cut

# given (10) {
#     when (10) { say("hello\n");continue}
#     when (3) { say("hello\n");continue}
#     when (4) { say("hello\n");continue}
#     default { say("hello\n"); continue}
# }
print("========三元表达式======\n");
$name="菜鸟教程";
$favorite=10;
$status=($favorite>20)?"热门网站":"非热门网站";
print($name.$status);
