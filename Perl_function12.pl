#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
use feature q(state);
=cut
Perl 子程序也就是用户定义的函数。
Perl 子程序即执行一个特殊任务的一段分离的代码，它可以使减少重复代码且使程序易读。
Perl 子程序可以出现在程序的任何地方，语法格式如下：
Perl 子程序可以和其他编程一样接受多个参数，子程序参数使用特殊数组 @_ 标明。
因此子程序第一个参数为 $_[0], 第二个参数为 $_[1], 以此类推。
不论参数是标量型还是数组型的，用户把参数传给子程序时，perl默认按引用的方式调用它们。
=cut
sub Average{
    #获取所有的参数
    $n=scalar(@_);#是为了获取参数的长度？接收参数是使用@_来接收还是其他
    $sum=0;
    foreach $item (@_){
        $sum+=$item;
    }
    $average=$sum/$n;
    print("传入的参数为：","@_\n");
    print("第一个参数为：$_[0]\n");
    print("传入参数的平均值：$average\n");
}
#调用函数
Average(10,20,30);
#向子程序中传递列表
# 由于 @_ 变量是一个数组，所以它可以向子程序中传递列表。
# 但如果我们需要传入标量和数组参数时，需要把列表放在最后一个参数上
sub PrintList{
    my @list=@_;
    print "传入的参数为：@list\n"
}
$a = 10;
@b = (1,2,3,4,5);
PrintList($a,@b);#把列表放在最后一个参数上

#向子程序传递哈希
sub PrintHash{
    my (%hash)=@_;
    foreach my $key (keys %hash){
        my $value=$hash{$key};
        print("传入哈希的值$key : $value\n");
    }
}
%hash=('name'=>'张三','age'=>25);
#传递哈希
PrintHash(%hash);
print("=======函数返回值=====复杂引用在下章\n");
sub add_a_b{
    $last=$_[0]+$_[1];
    #使用 return
    return("$last\n");
}
print(add_a_b(1,2));#直接传入参数
print("======函数的私有变量====\n");
=cut
默认情况下，Perl 中所有的变量都是全局变量，这就是说变量在程序的任何地方都可以调用。
如果我们需要设置私有变量，可以使用 my 操作符来设置。
my 操作符用于创建词法作用域变量，通过 my 创建的变量，存活于声明开始的地方，直到闭合作用域的结尾。
闭合作用域指的可以是一对花括号中的区域，可以是一个文件，也可以是一个 if, while, for, foreach, eval字符串。
sub somefunc{
    my $variable;#$variable 在方法somefunc()外不可见
    my ($another,@an_array,%a_hash)
}
=cut
#全局变量
$string="Hello,world";
#函数定义
sub printhello{
    my $string="你好,世界";
    print("声明的私有变量:$string\n");
}
printhello();
print("声明公共变量:$string\n");
print("====变量的临时赋值======\n");
=cut
我们可以使用 local 为全局变量提供临时的值，在退出作用域后将原来的值还回去。
local 定义的变量不存在于主程序中，但存在于该子程序和该子程序调用的子程序中。定义时可以给其赋值
=cut
#全局变量
$string2="hello world!";
sub PrintRunoob{
    #PrintHello 函数私有变量
    local $string2;
    $string2="hello,runoob";
    PrintMe();
    print "PrintRunoob 函数内字符串值：$string2\n";
}
sub PrintMe{
    print("PrintMe 函数内容字符串值:$string2\n");
}
sub PrintHello{
    print("PrintHello 函数内容的字符串值：$string2\n");
}
#函数调用
=cut
state操作符功能类似于C里面的static修饰符，state关键字将局部变量变得持久。
state也是词法变量，所以只在定义该变量的词法作用域中有效，举个例子：
注意：
    state仅能创建闭合作用域为子程序内部的变量
    state是从Perl 5.9.4开始引入的，所以使用前必须加上 use
    state可以声明标量、数组、哈希。但在声明数组和哈希时，不能对其初始化（至少Perl 5.14不支持）
=cut

PrintRunoob();
PrintHello();#默认传入的是上个函数里面的值？？
print("函数外部的字符串值：$string2\n");
print("======静态变量======\n");
sub PrintCount{
    state $count=0;#初始化变量；
    print "counter值为：$count\n";
    $count++;
}
for(1..5){
    PrintCount();
}
print("====子程序调用上下文=====\n");
=cut
子程序调用过程中，会根据上下文来返回不同类型的值，比如以下 localtime() 子程序，在标量上下文返回字符串，在列表上下文返回列表:
=cut

my $datestring=localtime(time);
print $datestring;
print "\n";
#列表上下文
($sec,$min,$hour,$mday,$mon, $year,$wday,$yday,$isdst) = localtime(time);
printf("%d-%d-%d %d:%d:%d",$year+1990,$mon+1,$mday,$hour,$min,$sec);
print "\n";