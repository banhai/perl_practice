#!/usr/bin/perl
use warnings FATAL => 'all';
use utf8;
binmode(STDOUT,":encoding(gbk)");
#======哈希======
=start
哈希是 key/value 对的集合。
Perl中哈希变量以百分号 (%) 标记开始。
访问哈希元素格式：${key}
=cut
%data=('google','google.com','baidu','baidu.com','taobao','taobao.com');
print("输出谷歌的网址$data{google}\n");#注意键值对的格式键的格式可以不用单引号
#=====创建哈希的格式======
print("=======创建哈希的格式======\n");
#第一种格式
$data2{google}='google.com';
$data2{taobao}='taobao.com';
$data2{baidu}='baidu.com';
print("$data2{baidu}\n");#不能直接使用输出这个格式的哈希格式
print("\n");
#第二种使用列表设置 列表第一元素为key,第二个为value
%data3=('google','google.com','runoob','runoob.com','taobao','taobao.com');
print("$data3{google}\n");
%data4=('google'=>'google.com','runoob'=>'runoob.com','taobao'=>'taobao.com');
print("$data4{google}\n");
%data5=(-google=>'google.com',-runoob=>'runboob.com',-taobao=>'taobao.com');#使用这种方式不能出现空格读取的方式为
print("$data5{-google}\n");
#访问哈希元素 访问元素格式：${key}进行获取
#读取哈希值可以像数组一样从哈希中提取值@{key1,key2}
@array=@data3{"google","runoob"};#此处必须使用引号进行包裹不论单双引号
print("Array:@array\n");
#读取哈希的key和value
print("==========读取哈希的key和value====\n");
#读取所有的key 格式如下：keys%Hash
@datakeys=keys %data3;
print("输出哈希的所有的key值=@datakeys\n");
#同样的方法可以读取所有的值使用 values%HASH
@datavalues=values %data3;
print("输出data3中所有哈希值@datavalues\n");
#检测元素是否存在：如果你在读取哈希中不窜在的键或者值的时候会返回undefined值并且执行时会有警告
#为避免这种情况我们可以使用exists函数来判断key/value对是否存在，存在的时候读取
if (exists($data3{ailibaba})) {#不用强调key值的引号
    print("data中存在这个数据\n");
}else{
    print("data中不存在这个数据\n")
}
#获取哈希的大小，其为元素的个数，我们可以通过先获取key或者value的所有元素数组，再计算数组元素的多少来获取哈希的大小
@keys=keys %data3;
$size=@keys;
print("1-哈希的大小：$size\n");
$values=values %data3;
print("2-哈希的大小：$size\n");
print("========哈希中添加或删除元素======\n");
$data3{dangdang}='dangdang.com';
$keys=keys %data3;
print("添加后的哈希长度$keys\n");
delete $data3{dangdang};
$size=keys %data3;
print("删除后的哈希长度=$size\n");
#foreach的遍历循环
foreach $key(keys %data3){
    print ("$data3{$key}\n");
}
#使用while的循环进行遍历输出
while(($key,$value)=each(%data3)){
    print("$data3{$key}\n");
}
