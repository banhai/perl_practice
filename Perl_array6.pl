#!/usr/bin/perl
use warnings FATAL => 'all';
use utf8;
binmode(STDOUT,":encoding(gbk)");
@hits=(2.3,25,64);
@names=("google","runoob","taobao");
print "\$hits[0]=$hits[0]\n";
print "\$hits[0]=$hits[1]\n";
print "\$hits[0]=$hits[2]\n";
print "\$names[0]=$names[0]\n";
print "\$names[1]=$names[1]\n";
print "\$names[2]=$names[2]\n";
#创建数组的两种方式
#第一种
@array1=(1,2,"hello");
#第二种
@array2=qw/这是 一个 数组/;
@array3=qw/
    张三
    李四
    王五
    马六
    赵七
/;
@tesarr=qw(张三 李四 王五);#中文不用带上双引号”“ 中间间隔不需要使用”，“隔开使用空格
print("测试的数组：@tesarr\n");
print "\@array=@array1\n";
print "\@array=@array2\n";
print "\@array=$array3[4]\n";#访问数组的元素使用$+数组名+[索引]
print "\@array=$array3[-1]\n";#反向索引读取最后一个
print "\@array=$array3[-2]\n";#反向索引读取最后一个
#数组序列号 ：Perl 提供了可以按序列输出的数组形式，格式为 起始值 + .. + 结束值
@var_arr=(10..20);#均包含首尾
@var_arr2=(20..30);
@var_abc=('a'..'z');

print "@var_arr\n";
print "@var_arr2\n";
print "@var_abc\n";

#数组的大小由数组种的标量上下文决定：
@array3=(1,2,3);
print "数组大小".scalar(@array3),"\n";
#添加和删除数组元素
=start
1 	push @ARRAY, LIST
将列表的值放到数组的末尾
2 	pop @ARRAY
删除数组的最后一个值
3 	shift @ARRAY
弹出数组第一个值，并返回它。数组的索引值也依次减一。相当与删除头一个元素
4 	unshift @ARRAY, LIST
将列表放在数组前面，并返回新数组的元素个数
=cut
print("---------------\n");
#创建一个简单的数组进行操作
@namearr=('张三',1,2,6);
print("\@namearr_push=@namearr\n");
push(@namearr,"李四");
print("\@namearr_pop=@namearr\n");
pop(@namearr);
print("\@namearr_shift=@namearr\n");
shift(@namearr);
print("\@namearr_shift=@namearr\n");
unshift(@namearr,"张三");
print("\@namearr_unshift=@namearr\n");
#切割数组
print("-----------\n");
@sits=qw/google taobao runoob weibo qq facebook 网易/;
@sites2=@sits[3,4,5];#截取的是sites中索引为3，4，5的值然后赋给新的数组
print("\@sites2=@sites2\n
----------
");
=kaisi
替换数组元素:Perl中数组元素替换使用 splice() 函数，语法格式如下
splice @ARRAY, OFFSET [ , LENGTH [ , LIST ] ]
    @ARRAY：要替换的数组。
    OFFSET：起始位置。
    LENGTH：替换的元素个数。
    LIST：替换元素列表。
=cut
@nums=(1..20);
print("替换之前=@nums\n");
splice(@nums,5,5,21..25);#(@ARRAY,OFFSET,LENGTH,LIST)
print("替换后=@nums\n");
#将字符串转换为数组
=e
Perl 中将字符串转换为数组使用 split() 函数，语法格式如下：
split [ PATTERN [ , EXPR [ , LIMIT ] ] ]
    PATTERN：分隔符，默认为空格。
    EXPR：指定字符串数。
    LIMIT：如果指定该参数，则返回该数组的元素个数。
=cut
#定义字符串
$var_test="runoob";
$var_String="www-baidu-com";
$var_names="google,taobao,baidu,weibo";
$www="www.taobao.com";
#-------字符串转为数组-------
print("----------\n");
print("runoob转化之前=$var_test\n");
@test1=split('',$var_test);
print("转化后\@test1=@test1\n");
print("www-baidu-com转化之前=$var_String\n");#使用.进行分割的时候为什么执行不了
@test2=split('-',$var_String);#为什么.不能进行分割
print("转化后\@test2=@test2\n");
print("转化之前=google,taobao,baidu,weibo\n");
@test3=split(',',$var_names,2);
print("转化后test3返回指定的次数=@test3\n");
#------使用.进行分割出现错误-------
@test4=split('.',$www);
print("\@test4=@test4\n");#不能进行正确的分割

#------将数组转换为字符串--------
$var_String="www-baidu-com";
$var_names="goole,taobao,baidu,ailibaba";
#字符串转为数组
@string1=split("-",$var_String);
print("转换后的数组:@string1\n");
print("逐个输出：$string1[1]");
@string2=split(",",$var_names);
print("转换后的数组:@string2\n");
print("逐个输出：$string2[2]\n");
#将字符串转化为数组
$str_arr1=join("-",@string1);
print("转化后的首个字符串=$str_arr1\n");
$str_arr2=join(",",@string2);
print("转化后第二个字符串=$str_arr2\n");
#========数组排序===========
=start
数组排序是根据Ascll数字值来排序。所以我们在对数组进行排序时最好先将每个元素转换为小写后在进行排序
=cut
print("========数组排序===========\n");
@sites=qw(google taobao baidu facebook);
print("@sites\n");
@sites2=sort(@sites);
print("排序后@sites2\n");
#=====特殊变量新版本已经废弃======
print("=====特殊变量新版本已经废弃======\n");
#特殊变量 $[ 表示数组的第一索引值，一般都为 0 ，如果我们将 $[ 设置为 1，则数组的第一个索引值即为 1，第二个为 2，以此类推。实例如下：
@arrtest=qw(1 2 2 3 4 5 6);
print("数组@arrtest\n");
#设置数组的第一索引为1
=tt

$[=1;#代码出现错误
print("第一个索引值为$arrtest[1]");
print("第二个索引值为$arrtest[2]\n");
=cut
#=====合并数组=======
print("=====合并数组=======\n");
@numbers=(1,3,(4,5,6));#以逗号分割，也可以使用都好合并数组
print "nubers=@numbers\n";
#数组中嵌入多个数组，并合并到主数组
@odd=(1,2,3);
@even=(2,4,6);
@numbers2=(@odd,@even);
print("合并后的新数组并不进行去重=@numbers2\n");
#===从列表中选择元素====一个列表可以当作一个数组使用，在列表后指定索引值可以读出指定的元素
$var=(5,4,3,2,1)[4];
print("var的值为=$var\n");
$var2=(1,2,3,4,5,6,7,8,9,10)[1..6];
print("var2的值为=$var2\n");
#在数组中使用..来读取指定范围的元素
@list=(5,4,3,2,1)[1..2];#包含首尾
print("截取的数组为@list");
