#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
use POSIX qw(strftime);
=st
time()：函数返回从1970年一月一日起累计的秒数
localtime()函数：获取本地市区时间
gmtime()函数：获取格林威治时间
locltime()函数在没有参数的情况下返回当前时间和日期
sec,     # 秒， 0 到 61  #不因该也是到59吗？
min,     # 分钟， 0 到 59
hour,    # 小时， 0 到 24
mday,    # 天， 1 到 31
mon,     # 月， 0 到 11
year,    # 年，从 1900 开始
wday,    # 星期几，0-6,0表示周日
yday,    # 一年中的第几天,0-364,365
isdst    # 如果夏令时有效，则为真
=cut
@months = qw( 一月 二月 三月 四月 五月 六月 七月 八月 九月 十月 十一月 十二月 );
@days = qw(星期天 星期一 星期二 星期三 星期四 星期五 星期六);

@date1=($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
print "$mday $months[$mon] $days[$wday]\n";
print(localtime()."直接调用localtime函数\n");
foreach $one (@date1){
    print("$one——")#此时夏令时为假的所以为0
}
print("\n秒-分钟-小时-天-月-年-星期几-年的第几天-夏令时是否有效\n");#除天和年数外其余均从索引0开始
$local_datatime=localtime();
print("当前时间：$local_datatime\n");
$gmt_data=gmtime();
print("格林威治时间为：$gmt_data\n");#格林威治时间与北京时间相差8个小时，格林时间+8小时是北京时间
printf("格式化时间：HH-MM-SS\n");
printf("%02d:%02d:%02d\n",$hour,$min,$sec);
print("=====新纪元时间====\n");
$epoc=time();
print("从1970年1月1日起累计的秒数为：$epoc\n");
print("=====时间格式====\n");
print "当期时间和日期：";
printf("%d-%d-%d %d:%d:%d",$year+1900,$mon+1,$mday,$hour,$min,$sec);
print("\n");
$epmc=time();
$epoc=$epoc-24*60*60;#获取的是一天前的秒数
@date1=($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime($epoc);
print("昨天的时间和日期");
printf("%d-%d-%d %d:%d:%d\n",$year+1900,$mon+1,$mday,$hour,$min,$sec);
#POSIX 函数 strftime()
=str
带*号的需要依赖本地时间
     符号                 描述                     实例
    %a 	星期几的简称（ Sun..Sat） * 	         Thu
    %A 	星期几的全称（ Sunday..Saturday） * 	Thursday
    %b 	月的简称（Jan..Dec） * 	         Aug
    %B 	月的全称（January..December） * 	  August
    %c 	日期和时间 * 	Thu Aug 23 14:55:02   2001
    %C 	年份除于100，并取整 (00-99) 	     20
    %d 	一个月的第几天 (01-31) 	         23
    %D 	日期, MM/DD/YY 相等于%m/%d/%y 	08/23/01
    %e 	一个月的第几天，使用空格填充个位数 ( 1-31) 	23
    %F	YYYY-MM-DD 的简写类似于 %Y-%m-%d 	  2001-08-23
    %g 	年份的最后两位数 (00-99) 	           01
    %g 	年 	2001
    %h 	月的简称 * (和%b选项相同) 	            Aug
    %H	24 小时制 (00-23) 	                14
    %I 	12 小时制 (01-12) 	                02
    %j 	一年的第几天 (001-366) 	            235
    %m 	月 (01-12) 	                        08
    %M 	分钟 (00-59) 	                    55
    %n 	新行 ('\n')
    %p	显示出AM或PM 	                    PM
    %r 	时间（hh：mm：ss AM或PM），12小时 * 	02:55:02 pm
    %R 	24 小时 HH:MM 时间格式,相等于 %H:%M 	14:55
    %S 	秒数 (00-61) 	02
    %t 	水平制表符 ('\t')
    %T 	时间（24小时制）（hh:mm:ss），相等于%H:%M:%S 	14:55
    %u 	ISO 8601 的星期几格式，星期一为1 (1-7) 	    4
    %U 	一年中的第几周，星期天为第一天(00-53) 	    33
    %V 	ISO 8601 第几周 (00-53) 	34
    %w 	一个星期的第几天（0代表星期天） (0-6) 	    4
    %W 	一年的第几个星期，星期一为第一天 (00-53) 	    34
    %x 	显示日期的格式（mm/dd/yy） * 	        08/23/01
    %X 	显示时间格式 * 	                    14:55:02
    %y 	年，两位数 (00-99) 	                    01
    %Y 	年 	                                   2001
    %z 	ISO 8601与UTC的时区偏移(1 minute=1, 1 hour=100)    +100
    %Z 	当前时区的名称,如"中国标准时间" *             CDT
    %% 	        % 符号 	                                 %
=cut
$datestring=strftime "%Y-%m-%d %H:%M:%S",localtime;
printf("格式化后的时间日期-$datestring\n");
#GMT格式化时间日期
$datestring=strftime "%Y-%m-%d %H:%M:%S",gmtime;
printf("格式化格林时间日期-$datestring\n");
