#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
#Perl提供了多种处理错误方法
# if(open(DATA, $file)){
#     print("打开文件的上下文");
# }else{
#     die "Error: 无法打开文件 - $!";
# }
#简单的写法为
#open(DATA,$file)||die "error: 无法打开文件-$!";
#unless函数中使用unless函数与if相反，只有在表达式返回时才会执行
# unless(chdir("/etc")){
#     die "Error:无法打开目录 -$!"
# }
#上面的代码简写
# die "Error: 无法打开目录!: $!" unless(chdir("/etc"));

#三元运算符
# 下面是一个三元运算符的简单实例
# print(exists($hash{keys}))?'存在':'不存在',"\n";
#*****warn函数用于出发一个警告信息，不会有其他操作，输出到STDERR(标准输出文件),通常用于给用户提示：
# chdir('/etc') or warn "无法切换目录";
# die函数类似于warn,但它会执行退出。一般用作错误信息的输出：
# chdir('/etc') or die"无法切换目录";


=cut
在 Perl 脚本中，报告错误的常用方法是使用 warn() 或 die() 函数来报告或产生错误。
而对于 Carp 模块，它可以对产生的消息提供额外级别的控制，尤其是在模块内部。
标准 Carp 模块提供了 warn() 和 die() 函数的替代方法，
它们在提供错误定位方面提供更多信息，而且更加友好。当在模块中使用时，错误消息中包含模块名称和行号。
=cut

# Carp模块的内容
# carp函数可以输出程序的跟踪信息，类似于 warn 函数，通常会将该信息发送到 STDERR：
package A;

require Exporter;
@ISA = qw/Exporter/;
@EXPORT = qw/function/;
use Carp;

sub function1 {
    carp "Error in module!";
}
1;
function1();
#cluck函数 cluck()与warn类似，提供了从产生错误处的栈回溯追踪
package T;

require Exporter;
@ISA = qw/Exporter/;
@EXPORT = qw/function/;
use Carp qw(cluck);

sub function {
    cluck "Error in module!";
}
1;
function();
# croak 函数
# croak() 与 die() 一样，可以结束脚本
# package B;
# require Exporter;
# @ISA = qw/Exporter/;
# @EXPORT = qw/function/;
# use Carp;
# sub function {
#     croak "Error in module!";
# }
# 1;
# function();
#confess函数
# confess()与die()类似，但提供从生产错误处的栈回溯追踪
package C;
require Exporter;
@ISA = qw/Exporter/;
@EXPORT = qw/function/;
use Carp;
sub functionC {
    confess "Error in module!";
}
1;
functionC();