package Person;
use strict;
use utf8;
binmode(STDOUT,":encoding(gbk)");
sub new{
    my $class=shift;
    my $self={
        _firstName=>shift,
        _lastaName=>shift,
        _ssn=>shift,
    };
    # 输出用户信息
    print("名字:$self->{_firstName}\n");
    print("姓氏:$self->{_lastaName}\n");
    print("编号:$self->{_ssn}\n");
    bless $self,$class;
    return $self;
}
sub setFirstName{
    my($self,$firstName)=@_;
    $self->{_firstName}=$firstName if defined($firstName);
    return($self->{_firstName});
}
sub getFirstName{
    my($self)=@_;
    retrun $self->{_firstName};
}
1;
