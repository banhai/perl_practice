#!/usr/bin/perl
use utf8;
binmode(STDOUT,":encoding(gbk)");
=cut
opendir DIRHANDLE, EXPR  # 打开目录
readdir DIRHANDLE        # 读取目录
rewinddir DIRHANDLE      # 定位指针到开头
telldir DIRHANDLE        # 返回目录的当前位置
seekdir DIRHANDLE, POS   # 定位指定到目录的 POS 位置
closedir DIRHANDLE       # 关闭目录
=cut

#显示/t目录下的所有文件
$dir="t/*";
my @files=glob($dir);
foreach(@files){
    print $_."\n";
}
print("=====显示所有.pl结尾的文件========\n");
#显示/temp目录下所有以.pl结尾的文件
$dir="t/*.pl";
#$dir="t/.*”   查看所有隐藏的文件
my @files=glob($dir);
foreach(@files){
    print $_."\n";
}
#显示/tem和/home目录下的所有文件
print("======显示多个文件夹下的目录=======\n");
$dir =" lib/*   t/*";
@failes=glob($dir);
foreach(@failes){
    print $_."\n";
}
print("=====当前目录下的所有==\n");
# 以下的实例可以列出当前目录下的所有文件
opendir(DIR,'.') or die "无法打开目录,$!";
while($file=readdir DIR){
    print "$file\n";
}
closedir DIR;
#如果要显示目录下的所有.txt结尾的文件，可以使用一下代码
print("=======当前目录下的所有以.txt结尾的文件=======");
opendir(DIR,'.') or die "无法打开目录,$!";
foreach(sort grep(/^.*\.txt$/,readdir(DIR))){
    print("$_\n");
}
closedir DIR;
print("======创建一个目录======\n");
#可以使用mkdir函数创建一个新目录，执行前你需要有足够的权限来创建目录；
$dir="perl/";
mkdir($dir) or die "无法创建$dir目录，$!";
print("目录创建成功\n");
#删除目录
print("========删除目录======\n");
# $dir="perl/";
# #删除主目录下的perl目录
# rmdir($dir) or die "无法删除 $dir 目录,$!";
# print("目录删除成功");
